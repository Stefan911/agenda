package org.magenpurp.stefan911.agenda.system;

// add 1234 Stefan email
//  0   1     2     3


public class ListManager {
    public void executeAction(String[] arg) {
        switch (arg[0].toLowerCase()) {
            case "add": {
                if (arg.length >= 3) {
                    if (Persoana.getPerson(arg[1]) != null) {
                        System.out.println("➽ You already have someone saved with that number.");
                    }
                    else {
                        Persoana.generatePerson(arg[1], arg[2], (arg.length >= 4 ? arg[3] : null));
                        System.out.println("➽ Operation `add` successful.");
                    }
                }
                else {
                    System.out.println("➽ Incorrect format");
                }
                break;
            }
            case "del": {
                if (arg.length == 2) {
                    String number = arg[1];

                    Persoana pers = Persoana.getPerson(number);
                    if (pers != null) {
                        pers.delete();
                        System.out.println("➽ Operation `del` successful.");
                    }
                    else {
                        System.out.println("➽ Person not found.");
                    }
                }
                else {
                    System.out.println("➽ Incorrect format");
                }
                break;
            }
            case "find": {
                if (arg.length == 2) {
                    String number = arg[1];

                    Persoana pers = Persoana.getPerson(number);
                    if (pers != null) {
                        pers.print();
                    }
                    else {
                        System.out.println("➽ Person not found.");
                    }
                }
                else {
                    System.out.println("➽ Incorrect format");
                }
                break;
            }
            case "exit": {
                System.out.println("➽ Exiting... Thank you!");
                return;
            }
            case "exiterr": {
                System.out.println("➽ Force exiting because of an error...");
                return;
            }
            //TODO: Function to list all cached people
            default: {
                System.out.println("➽ Incorrect format");
                break;
            }
        }
    }
}
