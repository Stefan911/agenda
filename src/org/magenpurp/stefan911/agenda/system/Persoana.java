package org.magenpurp.stefan911.agenda.system;

import java.util.HashMap;

import static org.magenpurp.stefan911.agenda.Main.*;

public class Persoana {
    private String number;
    private String name;
    private String email;

    public static HashMap<String, Persoana> cache = new HashMap<>();

    private Persoana(String number, String name, String email) {
        this.number = number;
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getNumber() {
        return number;
    }

    public void print() {
        System.out.println("------------------------------");
        System.out.println("NAME: " + name);
        System.out.println("NUMBER: " + number);
        System.out.println("EMAIL: " + (email == null ? "NOT SPECIFIED" : email));
        System.out.println("------------------------------");
    }

    public void delete() {
        cache.remove(number);
        files.set(number, null);
    }

    /**
     * Rescriere fisier yml la inchidere de program din cache
     * Sortare dupa nume a hashmap-ului
     */

    public static Persoana generatePerson(String number, String name, String email) {
        if (number == null || name == null) throw new RuntimeException("Invalid constructor.");

        if (cache.containsKey(number)) return cache.get(number);

        Persoana pers = new Persoana(number, name, email);
        cache.put(number, pers);
        if (!files.contains(number)) {
            files.set(number + ".Name", name);

            if (email != null) {
                files.set(number + ".Email", email);
            }
        }
        return pers;
    }

    public static Persoana getPerson(String number) {
        if (cache.containsKey(number)) {
            return cache.get(number);
        }
        else if (files.contains(number)) {
            return generatePerson(number, files.getString(number + ".Name"), (files.contains(number + ".Email") ? files.getString(number + ".Email") : null));
        }
        return null;
    }


}
