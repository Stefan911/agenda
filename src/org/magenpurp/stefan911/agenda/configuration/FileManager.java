package org.magenpurp.stefan911.agenda.configuration;


import java.io.File;
import java.io.IOException;
import java.util.List;


public class FileManager {
    private Configuration yml;
    private File file;

    public FileManager(String name, String path) {
        initialize(name, path);
    }

    public FileManager(String name) {
        initialize(name, "");
    }

    private void initialize(String name, String path) {
        path = "files/" + path;
        new File(path).mkdirs();
        file = new File(path, name + ".yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        try {
            yml = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(path, name + ".yml"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    public static boolean exists(String name, String path) {
        return new File("files/" + path + "/", name + ".yml").exists();
    }

    public static File getFile(String name, String path) {
        return new File("files/"  + path + "/", name + ".yml");
    }

    public void save() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(yml, new File(file.getParentFile().getPath(), file.getName()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void set(String path, Object object) {
        yml.set(path, object);
        save();
    }


    public boolean contains(String path) {
        return yml.contains(path);
    }

    public List<String> getStringList(String path) {
        return yml.getStringList(path);
    }

    public String getString(String path) {
        return yml.getString(path);
    }

    public double getDouble(String path) {
        return yml.getDouble(path);
    }

    public void addDefault(String path, Object object) {
        if (!contains(path)) {
            yml.set(path, object);
        }
        save();
    }

    public boolean getBoolean(String path) {
        return yml.getBoolean(path);
    }

    public int getInt(String path) {
        return yml.getInt(path);
    }

    public long getLong(String path) {
        return yml.getLong(path);
    }


    public Object getObject(String path) {
        return yml.get(path);
    }
}