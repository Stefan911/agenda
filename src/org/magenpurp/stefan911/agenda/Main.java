package org.magenpurp.stefan911.agenda;

import org.magenpurp.stefan911.agenda.configuration.FileManager;
import org.magenpurp.stefan911.agenda.system.ListManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Agenda telefonica prin intermediul interfetei consolei
 * Limbaj utilizat: Java
 * Comenzi:
 * <> - argumente optionale, [] - argumente obligatorii
 * - add [numar telefon] [nume] <email>
 * - del [numar telefon]
 * - find [numar telefon]
 * - exit
 */

public class Main {
    public static FileManager files;
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static String read() {
        try {
            return reader.readLine();
        } catch (Exception e) {
            e.printStackTrace();
            return "exiterr";
        }
    }

    public static void main(String[] args) {
        files = new FileManager("Cache");
        ListManager listManager = new ListManager();

        String s = "";

        while (!s.equalsIgnoreCase("exit")) {
            System.out.println("➽ Please insert action");
            s = read();
            listManager.executeAction(s.split(" "));
        }
    }
}
